<?php

use App\Http\Controllers\SpaController;
use Illuminate\Support\Facades\Route;

Route::get('/', [SpaController::class, 'admin']);
Route::get('/{any}', [SpaController::class, 'admin'])->where('any', '.*');
