<?php

use App\Http\Controllers\SpaController;
use Illuminate\Support\Facades\Route;

Route::get('/', [SpaController::class, 'seller']);
Route::get('/{any}', [SpaController::class, 'seller'])->where('any', '.*');
