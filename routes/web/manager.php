<?php

use App\Http\Controllers\SpaController;
use Illuminate\Support\Facades\Route;

Route::get('/', [SpaController::class, 'manager']);
Route::get('/{any}', [SpaController::class, 'manager'])->where('any', '.*');
