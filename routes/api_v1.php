<?php

use App\Http\Controllers\Api\V1\Seller\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(__DIR__ . '/apis/auth/auth.php');
Route::prefix('admin')->middleware(['auth:api', 'check.role:admin'])->group(__DIR__ . '/apis/api_v1/admin.php');
Route::prefix('manager')->middleware(['auth:api', 'check.role:manager'])->group(__DIR__ . '/apis/api_v1/manager.php');
Route::prefix('seller')->middleware(['auth:api', 'check.role:seller'])->group(__DIR__ . '/apis/api_v1/seller.php');
Route::prefix('site')->middleware('auth:api')->group(__DIR__ . '/apis/api_v1/site.php');



