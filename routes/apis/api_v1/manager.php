<?php

use App\Http\Controllers\Api\V1\Manager\CompanyController;
use App\Http\Controllers\Api\V1\Manager\CustomerController;
use App\Http\Controllers\Api\V1\Manager\EmployeeController;
use App\Http\Controllers\Api\V1\Manager\OrderController;
use App\Http\Controllers\Api\V1\Manager\UserController;
use Illuminate\Support\Facades\Route;


Route::prefix('users')->group(function () {
    Route::get('/user', [UserController::class, 'authUser']);
});
Route::prefix('companies')->group(function () {
    Route::get('/search/{name}', [CompanyController::class, 'search']);
});
Route::prefix('employees')->group(function () {
    Route::resource('employees', EmployeeController::class)->except([
        'create', 'edit'
    ]);
});
Route::prefix('customers')->group(function () {
    Route::apiResource('customers', CustomerController::class)->except([
        'create', 'edit'
    ]);
    Route::get('/customer/{phone}', [CustomerController::class, 'getCustomerByPhone']);
});

