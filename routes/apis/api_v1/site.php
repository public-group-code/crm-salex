<?php

use App\Http\Controllers\Api\V1\Site\UserController;
use Illuminate\Support\Facades\Route;


Route::prefix('users')->group(function () {
    Route::resource('users', UserController::class)->except([
        'create', 'edit'
    ]);
    Route::get('/user', [UserController::class, 'authUser']);
});

