<?php

use App\Http\Controllers\Api\V1\Seller\AdvertisingController;
use App\Http\Controllers\Api\V1\Seller\CommentController;
use App\Http\Controllers\Api\V1\Seller\CompanyController;
use App\Http\Controllers\Api\V1\Seller\CustomerController;
use App\Http\Controllers\Api\V1\Seller\OrderController;
use App\Http\Controllers\Api\V1\Seller\ProductController;
use App\Http\Controllers\Api\V1\Seller\UserController;
use Illuminate\Support\Facades\Route;


Route::prefix('users')->group(function () {
    Route::resource('users', UserController::class)->except([
        'create', 'edit'
    ]);
    Route::get('/user', [UserController::class, 'authUser']);
});
Route::prefix('orders')->group(function () {
    Route::resource('orders', OrderController::class)->except([
        'create', 'edit'
    ]);
    Route::get('count_orders', [OrderController::class, 'countOrders']);
});
Route::prefix('products')->group(function () {
    Route::resource('products', ProductController::class)->except([
        'create', 'edit'
    ]);
});
Route::prefix('comments')->group(function () {
    Route::resource('comments', CommentController::class)->except([
        'create', 'edit'
    ]);
});
Route::prefix('customers')->group(function () {
    Route::apiResource('customers', CustomerController::class)->except([
        'create', 'edit'
    ]);
    Route::get('/customer/{phone}', [CustomerController::class, 'getCustomerByPhone']);
});
Route::prefix('advertisings')->group(function () {
    Route::resource('advertisings', AdvertisingController::class)->except([
        'create', 'edit'
    ]);
});

