<?php

namespace App\Providers;

use App\Contracts\RoleInterface;
use App\Repositories\RoleRepository;
use Illuminate\Support\ServiceProvider;

class RoleServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            RoleInterface::class,
            RoleRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Получить сервисы от провайдера.
     *
     * @return array
     */
    public function provides()
    {
        return [RoleInterface::class];
    }
}
