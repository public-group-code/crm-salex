<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'name', 'purchase_price', 'price', 'quantity'];
    /**
     * @return BelongsTo
     */
    public function user():BelongsTo
    {
        return $this->BelongsTo(User::class);
    }

    /**
     * @return BelongsToMany
     */
    public function orders():BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }
}
