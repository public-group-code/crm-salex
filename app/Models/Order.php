<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'customer_id',
        'prepayment',
        'discount',
        'cash_on_delivery',
        'status',
        'comment_id',
        'profit',
    ];

    /**
     * @return BelongsTo
     */
    public function customer():BelongsTo
    {
        return $this->BelongsTo(Customer::class);
    }

    /**
     * @return BelongsToMany
     */
    public function products():BelongsToMany
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity_order_product');
    }

    /**
     * @return BelongsTo
     */
    public function comment():BelongsTo
    {
        return $this->BelongsTo(Comment::class);
    }
}
