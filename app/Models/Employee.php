<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'users';

    const ALLOWED_ROLES_REGISTRATION = [
        'manager', 'seller'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'company_id',
        'name',
        'email',
        'phone',
        'salary_type',
        'salary_percent',
        'salary_rate',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }
    /**
     * @return BelongsToMany
     */
    public function companies():BelongsToMany
    {
        return $this->belongsToMany(Company::class);
    }

    /**
     * @return HasManyThrough
     */
    public function orders(): HasManyThrough
    {
        return $this->hasManyThrough(Order::class, Customer::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function advertisings(): HasMany
    {
        return $this->hasMany(Advertising::class, 'user_id');
    }

    public function getCountOrdersCompleted(): int
    {
        $orders = $this->orders()->where('orders.status', '=', 'Completed')->get();

        return $orders->count();
    }

    /**
     * @return integer
     */
    public function proceedsYear(): int
    {
        return $this->orders()
            ->whereYear('orders.created_at', Carbon::today()->year)
            ->sum('profit');
    }

    /**
     * @return integer
     */
    public function proceedsSubMonth(): int
    {
        $subMonth = Carbon::now()->startOfMonth()->subMonth();

        return $this->orders()
            ->where('orders.status', '=', 'Completed')
            ->whereMonth('orders.created_at', $subMonth)
            ->sum('profit');
    }

    /**
     * @return integer
     */
    public function advertisingCostYear(): int
    {
        $advertisingYear = $this->advertisings()
           ->whereYear('advertisings.created_at', Carbon::today()->year)
           ->first();

        if ($advertisingYear){
            return  $advertisingYear->january +
                    $advertisingYear->february +
                    $advertisingYear->march +
                    $advertisingYear->april +
                    $advertisingYear->may +
                    $advertisingYear->june +
                    $advertisingYear->july +
                    $advertisingYear->august +
                    $advertisingYear->september +
                    $advertisingYear->october +
                    $advertisingYear->november +
                    $advertisingYear->december;
        }else{
            return 0;
        }
    }

    /**
     * @return integer|null
     */
    public function advertisingCostSubMonth(): ?int
    {
        $advertisingYear = $this->advertisings()
            ->whereYear('advertisings.created_at', Carbon::today()->year)
            ->first();

        if ($advertisingYear){
            $subMonth = strtolower(Carbon::now()->startOfMonth()->subMonth()->format('F'));

            return $advertisingYear->{$subMonth};
        }else{
            return null;
        }
    }

    /**
     * @return int
     */
    private function salarySubMonth($proceedsSubMonth): int
    {
        if ($this->salary_type === 'salary_percent'){
            return $proceedsSubMonth / 100 * $this->salary_percent;
        }else if ($this->salary_type === 'salary_rate'){
            return $this->salary_rate;
        }
    }

    /**
     * @return int
     */
    public function efficiency(): int
    {
        $proceedsSubMonth = $this->proceedsSubMonth();

        $advertisingCostSubMonth = $this->advertisingCostSubMonth();

        $salarySubMonth = $this->salarySubMonth($advertisingCostSubMonth);

        return $proceedsSubMonth - $advertisingCostSubMonth - $salarySubMonth;
    }
}
