<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository implements \App\Contracts\RoleInterface
{
    public function rolesIds($request)
    {
       return Role::whereIn('slug', $request->roles)->pluck('id');
    }
}
