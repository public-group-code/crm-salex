<?php

namespace App\Repositories;

use App\Models\Employee;
use Illuminate\Support\Facades\Hash;

class EmployeeRepository implements \App\Contracts\EmployeeInterface
{

    public function getEmployeesAll($user)
    {
        $company = $user->company;

        return Employee::where('id', '!=', $user->id)->where('company_id', $company->id)->paginate(20);
    }

    public function createEmployee($user, $request)
    {
        return Employee::create(
            array_merge(
                $request->validated(),
                [
                    'company_id' => $user->company_id,
                    'password' => Hash::make($request->password)
                ]
            )
        );
    }

    public function updateEmployee($employee, $request)
    {
        return $employee->update(
            array_merge(
                $request->validated(),
                ['password' => Hash::make($request->password)]
            )
        );
    }
}
