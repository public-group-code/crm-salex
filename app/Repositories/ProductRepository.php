<?php

namespace App\Repositories;

class ProductRepository implements \App\Contracts\ProductInterface
{
    public function getProductsAll($user)
    {
        return $user->products()->paginate(20);
    }

    public function attachProductsToOrder($order, $data)
    {
        foreach($data['products'] as $product){
            $order->products()->attach(
                $product['id'],
                ['quantity_order_product' => $product['quantity_order_product']]
            );
        }
    }
}
