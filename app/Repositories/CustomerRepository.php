<?php

namespace App\Repositories;

use App\Models\Customer;

class CustomerRepository implements \App\Contracts\CustomerInterface
{

    public function getCustomersAll($user)
    {
        return $user->customers()->orderBy('created_at', 'desc')->paginate(20);
    }

    public function getCustomerByPhone($user, $phone)
    {
        return Customer::where('user_id', $user->id)->where('phone', $phone)->first();
    }

    public function getOrCreateCustomer($user, $data)
    {
        $customer = Customer::where('user_id', $user->id)
            ->where('phone', $data['phone'])
            ->first();

        if ($customer){
            return $customer;
        }else{
            return Customer::create(array_merge(
                $data,
                ['user_id' => $user->id]
            ));
        }
    }
}
