<?php

namespace App\Repositories;

use App\Models\Order;
use Carbon\Carbon;

class OrderRepository implements \App\Contracts\OrderInterface
{

    public function getOrdersAll($user)
    {
        return $user->orders()->orderBy('created_at', 'desc')->paginate(20);
    }

    public function createOrder($customer, $data)
    {
        return Order::create(array_merge(
            $data,
            ['customer_id' => $customer->id]
        ));
    }

    public function getCountOrdersDate($user, $date)
    {
        $orders = $user->orders()
            ->where('orders.created_at', '>', $date)
            ->get();

        $grouped = $orders->groupBy('status');

        return $grouped->map(function ($value) {
            return $value->count();
        });
    }

    public function getCountOrdersAllTime($user)
    {
        $orders = $user->orders()->get();

        $grouped = $orders->groupBy('status');

        return $grouped->map(function ($value) {
            return $value->count();
        });
    }
}
