<?php

namespace App\Http\Controllers;

class SpaController
{
    public function index()
    {
        return view('pages.site.index');
    }

    public function admin()
    {
        return view('pages.admin.index');
    }

    public function manager()
    {
        return view('pages.manager.index');
    }

    public function seller()
    {
        return view('pages.seller.index');
    }
}
