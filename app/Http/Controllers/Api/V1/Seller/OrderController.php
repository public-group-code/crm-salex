<?php

namespace App\Http\Controllers\Api\V1\Seller;

use App\Contracts\CustomerInterface;
use App\Contracts\OrderInterface;
use App\Contracts\ProductInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Customer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use function response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(OrderInterface $orderService): JsonResponse
    {
        return response()->json([
            'orders' => OrderResource::collection($orderService->getOrdersAll(Auth::user()))
                ->response()->getData()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return JsonResponse
     */
    public function store(CustomerInterface $customerService, OrderInterface $orderService, ProductInterface $productService, StoreOrderRequest $request): JsonResponse
    {
        $dataValidated = $request->validated();

        $user = Auth::user();

        $customer = $customerService->getOrCreateCustomer($user, $dataValidated);

        $createdOrder = $orderService->createOrder($customer, $dataValidated);

        $productService->attachProductsToOrder($createdOrder, $dataValidated);

        return response()->json([
            'order' => new OrderResource($createdOrder)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return JsonResponse
     */
    public function show(Order $order): JsonResponse
    {
        return response()->json([
            'order' => new OrderResource($order)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param OrderInterface $orderService
     * @return JsonResponse
     */
    public function countOrders(OrderInterface $orderService): JsonResponse
    {
        $OrdersDay = $orderService->getCountOrdersDate(Auth::user(), Carbon::today());
        $OrdersWeek = $orderService->getCountOrdersDate(Auth::user(), Carbon::today()->week);
        $OrdersMonth = $orderService->getCountOrdersDate(Auth::user(), Carbon::today()->month);
        $OrdersYear = $orderService->getCountOrdersDate(Auth::user(), Carbon::today()->year);
        $OrdersAll = $orderService->getCountOrdersAllTime(Auth::user());

        return response()->json([
            'count_orders' => [
                'day' => $OrdersDay,
                'week' => $OrdersWeek,
                'month' => $OrdersMonth,
                'year' => $OrdersYear,
                'all' => $OrdersAll,
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return JsonResponse
     */
    public function update(UpdateOrderRequest $request, Order $order): JsonResponse
    {
        //Data
        $dataValidated = $request->validated();

        //Current $user
        $user = Auth::user();

        //Customer
        $customer = Customer::where('user_id', $user->id)
            ->where('phone', $dataValidated['phone'])
            ->first();

        if ($customer){
            $customer->update($dataValidated);
            $customer_id = $customer->id;
        }else{
            $createdCustomer = Customer::create(array_merge(
                $dataValidated,
                ['user_id' => $user->id]
            ));
            $customer_id = $createdCustomer->id;
        }

        //Order
        $order->update(
            array_merge(
                $dataValidated,
                ['customer_id' => $customer_id]
            )
        );

        //Products
        $product_ids = [];

        foreach($dataValidated['products'] as $product){
            $product_ids[$product['id']] = ['quantity_order_product' => $product['quantity_order_product']];
        }

        $order->products()->sync(
            $product_ids
        );

        return response()->json([
            'order' => new OrderResource($order)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order): Response
    {
        $order->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
