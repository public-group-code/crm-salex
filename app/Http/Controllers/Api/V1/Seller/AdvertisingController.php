<?php

namespace App\Http\Controllers\Api\V1\Seller;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAdvertisingRequest;
use App\Http\Requests\UpdateAdvertisingRequest;
use App\Http\Resources\AdvertisingResource;
use App\Models\Advertising;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $advertisings = Auth::user()->advertisings()->orderBy('created_at', 'desc')->paginate(10);

        return response()->json([
            'advertisings' => AdvertisingResource::collection($advertisings)
                ->response()->getData()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAdvertisingRequest  $request
     * @return JsonResponse
     */
    public function store(StoreAdvertisingRequest $request)
    {
        $user = Auth::user();

        $createdAdvertising = Advertising::create(array_merge(
            $request->validated(),
            ['user_id' => $user->id]
        ));

        return response()->json([
            'advertising' => new AdvertisingResource($createdAdvertising)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Advertising  $advertising
     * @return JsonResponse
     */
    public function show(Advertising $advertising): JsonResponse
    {
        return response()->json([
            'advertising' => new AdvertisingResource($advertising)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertising  $advertising
     * @return JsonResponse
     */
    public function edit(Advertising $advertising): JsonResponse
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAdvertisingRequest  $request
     * @param  \App\Models\Advertising  $advertising
     * @return JsonResponse
     */
    public function update(UpdateAdvertisingRequest $request, Advertising $advertising): JsonResponse
    {
        $advertising->update($request->validated());

        return response()->json([
            'advertising' => new AdvertisingResource($advertising)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Advertising  $advertising
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertising $advertising)
    {
        $advertising->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
