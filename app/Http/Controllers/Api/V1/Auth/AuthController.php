<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $dataValid = $request->validated();

        $role = Role::where('slug', $request->role)->first();

        //Company
        if ($request->role == 'manager'){
            $createdCompany = Company::create([
                'name' => $request->company_name
            ]);
            $company_id = $createdCompany->id;
        }else{
            $company_id = $request->company_id;
        }

        //User
        $registeredUser = User::create(
            array_merge(
                $dataValid,
                [
                    'company_id' => $company_id,
                    'password' => Hash::make($request->password)
                ]
            )
        );

        $registeredUser->roles()->attach($role->id);

        $token = $registeredUser->createToken(config('app.name'));

        $token->token->expires_at = Carbon::now()->addDay();

        $token->token->save();

        return response()->json([
            'message' => ['You were successfully registered. Use your email and password to sign in.'],
            'token_type' => 'Bearer',
            'token' => $token->accessToken,
            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
            'code' => 200
        ], 200);
    }

    /**
     *  @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        $role = $request->cabinet;

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'You cannot sign with those credentials',
                'code' => 401
            ], 401);
        }

        if (!empty($role) && !Auth::user()->hasRole($role)) {
            return response()->json([
                'message' => 'You cannot sign with those credentials',
                'code' => 401,
            ], 401);
        }
        $token = Auth::user()->createToken(config('app.name'));

        $token->token->expires_at = $request->remember_me ?
            Carbon::now()->addMonth() :
            //Carbon::now()->addDay();
            Carbon::now()->addMinutes(1);

        $token->token->save();

        return response()->json([
            'message' => 'You have successfully logged in',
            'token_type' => 'Bearer',
            'token' => $token->accessToken,
            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
            'code' => 200
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'You are successfully logged out',
            'code' => 200
        ], 200);
    }
}
