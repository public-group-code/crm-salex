<?php

namespace App\Http\Controllers\Api\V1\Manager;

use App\Contracts\EmployeeInterface;
use App\Contracts\RoleInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Http\Resources\EmployeeResource;
use App\Models\Employee;
use App\Models\Role;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Http\JsonResponse;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(EmployeeInterface $employeeService): JsonResponse
    {
        $user = Auth::user();

        $employees = $employeeService->getEmployeesAll($user);

        return response()->json([
            'employees' => EmployeeResource::collection($employees)
                ->response()->getData()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEmployeeRequest  $request
     * @return JsonResponse
     */
    public function store(StoreEmployeeRequest $request,  RoleInterface $roleService, EmployeeInterface $employeeService): JsonResponse
    {
        $user = Auth::user();

        $rolesIds = $roleService->rolesIds($request);

        $createdEmployee = $employeeService->createEmployee($user, $request);

        $createdEmployee->roles()->attach($rolesIds);

        return response()->json([
            'employee' => $createdEmployee
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return JsonResponse
     */
    public function show(Employee $employee): JsonResponse
    {
        return response()->json([
            'employee' => new EmployeeResource($employee)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEmployeeRequest  $request
     * @param  \App\Models\Employee  $employee
     * @return JsonResponse
     */
    public function update(UpdateEmployeeRequest $request, Employee $employee,  RoleInterface $roleService, EmployeeInterface $employeeService): JsonResponse
    {
        $rolesIds = $roleService->rolesIds($request);

        $employeeService->updateEmployee($employee, $request);

        $employee->roles()->sync($rolesIds);

        return response()->json([
            'employee' => new EmployeeResource($employee)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
