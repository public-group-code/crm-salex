<?php

namespace App\Http\Controllers\Api\V1\Manager;

use App\Contracts\CustomerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use function response;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(CustomerInterface $customerService): JsonResponse
    {
        return response()->json([
            'customers' => CustomerResource::collection($customerService->getCustomersAll(Auth::user()))
                ->response()->getData()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCustomerRequest  $request
     * @return JsonResponse
     */
    public function store(StoreCustomerRequest $request): JsonResponse
    {
        $user = Auth::user();

        $createdCustomer = Customer::create(array_merge(
            $request->validated(),
            ['user_id' => $user->id]
        ));

        return response()->json([
            'customer' => new CustomerResource($createdCustomer)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return JsonResponse
     */
    public function show(Customer $customer): JsonResponse
    {
        return response()->json([
            'customer' => new CustomerResource($customer)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  $phone
     * @return JsonResponse
     */
    public function getCustomerByPhone(CustomerInterface $customerService, $phone): JsonResponse
    {
        $user = Auth::user();

        $customer = $customerService->getCustomerByPhone($user, $phone);

        if ($customer){
            return response()->json([
                'customer' => new CustomerResource($customer)
            ]);
        }else{
            return response()->json([
                'customer' => ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCustomerRequest  $request
     * @param  \App\Models\Customer  $customer
     * @return JsonResponse
     */
    public function update(UpdateCustomerRequest $request, Customer $customer): JsonResponse
    {
        $customer->update($request->validated());

        return response()->json([
            'customer' => new CustomerResource($customer)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer): Response
    {
        $customer->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
