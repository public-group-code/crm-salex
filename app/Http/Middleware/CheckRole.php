<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class CheckRole
{
    public function handle(Request $request, Closure $next, $role)
    {
        if ($request->user()->hasRole($role)) {
            return $next($request);
        }else{
            abort(401, 'Unauthorized');
        }
    }
}
