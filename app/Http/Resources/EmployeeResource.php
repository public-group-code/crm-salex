<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'roles' => RoleManagerResource::collection($this->roles),
            'salary_type' => $this->salary_type,
            'salary_percent' => $this->salary_percent,
            'salary_rate' => $this->salary_rate,
            'countOrdersCompleted' => $this->getCountOrdersCompleted(),
            'proceedsYear' => $this->efficiency(),
            'proceedsMonth' => $this->proceedsSubMonth(),
        ];
    }
}
