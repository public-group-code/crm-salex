<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer' => new CustomerResource($this->customer),
            'products' => OrderProductResource::collection($this->products),
            'prepayment' => $this->prepayment,
            'discount' => $this->discount,
            'cash_on_delivery' => $this->cash_on_delivery,
            'status' => $this->status,
            'comment' => new CommentResource($this->comment),
            'profit' => $this->profit,
        ];
    }
}
