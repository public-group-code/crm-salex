<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdvertisingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year' => ['required', 'integer', 'unique:advertisings'],
            'january' => ['nullable', 'integer'],
            'february' => ['nullable', 'integer'],
            'march' => ['nullable', 'integer'],
            'april' => ['nullable', 'integer'],
            'may' => ['nullable', 'integer'],
            'june' => ['nullable', 'integer'],
            'july' => ['nullable', 'integer'],
            'august' => ['nullable', 'integer'],
            'september' => ['nullable', 'integer'],
            'october' => ['nullable', 'integer'],
            'november' => ['nullable', 'integer'],
            'december' => ['nullable', 'integer'],
        ];
    }
}
