<?php

namespace App\Http\Requests;

use App\Models\Employee;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'email'.$this->id
            ],
            'phone' => ['nullable', 'string', 'max:255'],
            'salary_type' => ['nullable', 'string'],
            'salary_percent' => ['nullable', 'integer'],
            'salary_rate' => ['nullable', 'integer'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles' => ['required', 'array', 'max:255', Rule::in(Employee::ALLOWED_ROLES_REGISTRATION)],
        ];
    }
}
