<?php

namespace App\Contracts;

interface OrderInterface
{
    public function getOrdersAll($user);
    public function createOrder($customer, $data);
    public function getCountOrdersDate($user, $date);
    public function getCountOrdersAllTime($user);
}
