<?php

namespace App\Contracts;

interface CustomerInterface
{
    public function getCustomersAll($user);
    public function getCustomerByPhone($user, $phone);
    public function getOrCreateCustomer($user, $data);
}
