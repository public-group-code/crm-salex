<?php

namespace App\Contracts;

interface ProductInterface
{
    public function getProductsAll($user);
    public function attachProductsToOrder($order, $data);
}
