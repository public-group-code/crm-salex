<?php

namespace App\Contracts;

interface EmployeeInterface
{
    public function getEmployeesAll($user);
    public function createEmployee($user, $request);
    public function updateEmployee($employee, $request);
}
