export default {
    mutations:{
        //AUTH
        AUTH_REQUEST: (state) => {
            state.status = 'loading'
        },
        AUTH_SUCCESS: (state, {token}) =>{
            state.auth.status = 'authorized'
            state.auth.token = token
        },
        AUTH_ERROR: (state) => {
            state.auth.status = 'unauthorized'
        },
        LOGOUT: (state) => {
            state.auth.status = 'unauthorized'
            state.auth.token = ''
        },
        SET_AUTH_USER: (state, {user}) =>{
            state.users.user = user
        },
        //PRODUCTS
        SET_PRODUCTS: (state, {products, pagination}) =>{
            state.products.products = products
            state.products.pagination = pagination
        },
        SET_EDIT_PRODUCT: (state, {product}) =>{
            state.products.product = product
        },
        //COMMENTS
        SET_COMMENTS: (state, {comments, pagination}) =>{
            state.comments.comments = comments
            state.comments.pagination = pagination
        },
        SET_EDIT_COMMENT: (state, {comment}) =>{
            state.comments.comment = comment
        },
        //ORDERS
        SET_ORDERS: (state, {orders, pagination}) =>{
            state.orders.orders = orders
            state.orders.pagination = pagination
        },
        SET_COUNT_ORDERS: (state, {count_orders}) =>{
            state.orders.count_orders = count_orders
        },
        SET_EDIT_ORDER: (state, {order}) =>{
            state.orders.order = order
        },
        //CUSTOMERS
        SET_CUSTOMERS: (state, {customers, pagination}) =>{
            state.customers.customers = customers
            state.customers.pagination = pagination
        },
        SET_EDIT_CUSTOMER: (state, {customer}) =>{
            state.customers.customer = customer
        },
        //ADVERTISINGS
        SET_ADVERTISINGS: (state, {advertisings, pagination}) =>{
            state.advertisings.advertisings = advertisings
            state.advertisings.pagination = pagination
        },
        SET_COUNT_ADVERTISINGS: (state, {count_advertisings}) =>{
            state.advertisings.count_advertisings = count_advertisings
        },
        SET_EDIT_ADVERTISING: (state, {advertising}) =>{
            state.advertisings.advertising = advertising
        },
    }
}
