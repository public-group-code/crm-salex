export default {
    state: {
        //auth
        auth:{
            status: localStorage.getItem('token')? 'authorized' : 'unauthorized',
            token: localStorage.getItem('token') || '',
        },
        users:{
            user : {},
        },
        //products
        products:{
            products:[],
            product:{},
            pagination:{}
        },
        //comments
        comments:{
            comments:[],
            comment:{},
            pagination:{}
        },
        //orders
        orders:{
            orders:[],
            count_orders:[],
            order:{},
            pagination:{}
        },
        //customers
        customers:{
            customers:[],
            customer:{},
            pagination:{}
        },
        //Advertising
        advertisings:{
            advertisings:[],
            count_advertisings:[],
            advertising:{},
            pagination:{}
        }
    },
}
