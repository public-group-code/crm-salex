export default {
    getters : {
        //AUTH
        IS_LOGGED_IN: state => !!state.auth.token,
        TOKEN: state => state.auth.token,
        AUTH_STATUS: state => state.auth.status,
        AUTH_USER: state => state.users.user,
        //PRODUCTS
        PRODUCTS: state => state.products.products,
        PRODUCTS_PAGINATION: state => state.products.pagination,
        EDIT_PRODUCT: state => state.products.product,
        //ORDERS
        ORDERS: state => state.orders.orders,
        COUNT_ORDERS: state => state.orders.count_orders,
        ORDERS_PAGINATION: state => state.orders.pagination,
        EDIT_ORDER: state => state.orders.order,
        //COMMENTS
        COMMENTS: state => state.comments.comments,
        COMMENTS_PAGINATION: state => state.comments.pagination,
        EDIT_COMMENT: state => state.comments.comment,
        //CUSTOMERS
        CUSTOMERS: state => state.customers.customers,
        CUSTOMERS_PAGINATION: state => state.customers.pagination,
        EDIT_CUSTOMER: state => state.customers.customer,
        //ADVERTISINGS
        ADVERTISINGS: state => state.advertisings.advertisings,
        ADVERTISINGS_PAGINATION: state => state.advertisings.pagination,
        EDIT_ADVERTISING: state => state.advertisings.advertising,
    }
}
