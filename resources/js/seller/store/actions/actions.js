import axios from "axios";
import store from "../store";
import login from "../../../auth/login";
import register from "../../../auth/register";
import logout from "../../../auth/logout";

const AUTH = {...login, ...register, ...logout}

export default {
    actions:{
        //AUTH
        ...AUTH,
        GET_AUTH_USER_FROM_API({commit}){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/users/user',
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(resp => {
                        const user = resp.data.user;
                        commit('SET_AUTH_USER', {user});
                        return resolve(resp);
                    })
                    .catch(error => {
                        if (error.response.status === 401){
                            this.dispatch('LOGOUT');
                        }
                        return reject(error);
                    });
            })
        },
        //ORDERS
        GET_ORDERS_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/seller/orders/orders?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const orders = response.data.orders.data;
                    const pagination = {
                        links: response.data.orders.links,
                        meta: response.data.orders.meta,
                    }
                    commit('SET_ORDERS', {orders, pagination});
                })
                .catch(error => {
                    console.error(error)
                })
        },
        GET_ORDER_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/orders/orders/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const orderResp = response.data.order;
                        const order = {
                            id: orderResp.id,
                            status: orderResp.status,
                            first_name: orderResp.customer.first_name,
                            last_name: orderResp.customer.last_name,
                            phone: orderResp.customer.phone,
                            mail_address: orderResp.customer.mail_address,
                            products: orderResp.products,
                            prepayment: orderResp.prepayment,
                            discount: orderResp.discount,
                            cash_on_delivery: orderResp.cash_on_delivery,
                            comment_id: orderResp.comment !== null ? orderResp.comment.id : null,
                            profit: orderResp.profit,
                        }
                        commit('SET_EDIT_ORDER', {order})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_ORDER_TO_API({commit}, order){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/seller/orders/orders',
                    data: order,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        UPDATE_ORDER_TO_API({commit}, order){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/seller/orders/orders/' + order.id,
                    data: order,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_ORDER_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/seller/orders/orders/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        GET_COUNT_ORDERS_FROM_API({commit}){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/orders/count_orders',
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const count_orders = response.data.count_orders;
                        commit('SET_COUNT_ORDERS', {count_orders});
                        return resolve(response)
                    })
                    .catch(error => {
                        return reject(error)
                    })
            })
        },
        //PRODUCTS
        GET_PRODUCTS_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/seller/products/products?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const products = response.data.products.data;
                    const pagination = {
                        links: response.data.products.links,
                        meta: response.data.products.meta,
                    }
                    commit('SET_PRODUCTS', {products, pagination});
                })
                .catch(error => {
                })
        },
        GET_PRODUCT_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/products/products/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const productResp = response.data.product;
                        const product = {
                            id: productResp.id,
                            name: productResp.name,
                            purchase_price: productResp.purchase_price,
                            price: productResp.price,
                            quantity: productResp.quantity,
                        }
                        commit('SET_EDIT_PRODUCT', {product})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_PRODUCT_TO_API({commit}, product){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/seller/products/products',
                    data: product,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        UPDATE_PRODUCT_TO_API({commit}, product){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/seller/products/products/' + product.id,
                    data: product,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_PRODUCT_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/seller/products/products/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        //COMMENTS
        GET_COMMENTS_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/seller/comments/comments?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const comments = response.data.comments.data;
                    const pagination = {
                        links: response.data.comments.links,
                        meta: response.data.comments.meta,
                    }
                    commit('SET_COMMENTS', {comments, pagination});
                })
                .catch(error => {
                })
        },
        GET_COMMENT_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/comments/comments/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const commentResp = response.data.comment;
                        const comment = {
                            id: commentResp.id,
                            order: commentResp.order,
                            message: commentResp.message,
                        }
                        commit('SET_EDIT_COMMENT', {comment})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_COMMENT_TO_API({commit}, comment){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/seller/comments/comments',
                    data: comment,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        UPDATE_COMMENT_TO_API({commit}, comment){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/seller/comments/comments/' + comment.id,
                    data: comment,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_COMMENT_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/seller/comments/comments/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        //CUSTOMERS
        GET_CUSTOMERS_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/seller/customers/customers?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const customers = response.data.customers.data;
                    const pagination = {
                        links: response.data.customers.links,
                        meta: response.data.customers.meta,
                    }
                    commit('SET_CUSTOMERS', {customers, pagination});
                })
                .catch(error => {
                    console.error(error.response.data)
                })
        },
        GET_CUSTOMER_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/customers/customers/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const customerResp = response.data.customer;
                        const customer = {
                            id: customerResp.id,
                            first_name: customerResp.first_name,
                            last_name: customerResp.last_name,
                            phone: customerResp.phone,
                            mail_address: customerResp.mail_address,
                        }
                        commit('SET_EDIT_CUSTOMER', {customer})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        GET_CUSTOMER_BY_PHONE_FROM_API({commit}, phone){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/customers/customer/' + phone,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const customerResp = response.data.customer;
                        const customer = {
                            id: customerResp.id,
                            first_name: customerResp.first_name,
                            last_name: customerResp.last_name,
                            phone: customerResp.phone,
                            mail_address: customerResp.mail_address,
                        }
                        commit('SET_EDIT_CUSTOMER', {customer})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_CUSTOMER_TO_API({commit}, customer){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/seller/customers/customers',
                    data: customer,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        UPDATE_CUSTOMER_TO_API({commit}, customer){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/seller/customers/customers/' + customer.id,
                    data: customer,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_CUSTOMER_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/seller/customers/customers/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        //ADVERTISINGS
        GET_ADVERTISINGS_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/seller/advertisings/advertisings?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const advertisings = response.data.advertisings.data;
                    const pagination = {
                        links: response.data.advertisings.links,
                        meta: response.data.advertisings.meta,
                    }
                    commit('SET_ADVERTISINGS', {advertisings, pagination});
                })
                .catch(error => {
                    console.error(error.response.data)
                })
        },
        GET_ADVERTISING_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/seller/advertisings/advertisings/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const advertising = response.data.advertising;
                        commit('SET_EDIT_ADVERTISING', {advertising})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_ADVERTISING_TO_API({commit}, advertising){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/seller/advertisings/advertisings',
                    data: {
                        year: advertising.year,
                        january: advertising.months.january,
                        february: advertising.months.february,
                        march: advertising.months.march,
                        april: advertising.months.april,
                        may: advertising.months.may,
                        june: advertising.months.june,
                        july: advertising.months.july,
                        august: advertising.months.august,
                        september: advertising.months.september,
                        october: advertising.months.october,
                        november: advertising.months.november,
                        december: advertising.months.december,
                    },
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        UPDATE_ADVERTISING_TO_API({commit}, advertising){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/seller/advertisings/advertisings/' + advertising.id,
                    data: {
                        year: advertising.year,
                        january: advertising.months.january,
                        february: advertising.months.february,
                        march: advertising.months.march,
                        april: advertising.months.april,
                        may: advertising.months.may,
                        june: advertising.months.june,
                        july: advertising.months.july,
                        august: advertising.months.august,
                        september: advertising.months.september,
                        october: advertising.months.october,
                        november: advertising.months.november,
                        december: advertising.months.december,
                    },
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_ADVERTISING_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/seller/advertisings/advertisings/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
    }
}
