import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);


import vMainPage from '../components/pages/v-main-page'
import vLogin from '../components/auth/v-login'
import vRegister from '../components/auth/v-register'
import store from '../store/store'

const router = new Router({
    //mode: 'history',
    routes: [
        {
            path: '/',
            name: 'mainPage',
            component: vMainPage,
        },
        {
            path: '/login',
            name: 'login',
            component: vLogin
        },
        {
            path: '/register',
            name: 'register',
            component: vRegister
        },
    ],
    linkExactActiveClass: "active",
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        store.dispatch('GET_AUTH_USER_FROM_API')
            .then(response => {
                //If the user is logged in
                next()
            }).catch(error => {
            //If the user is unauthorized
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        });

    } else {
        next()
    }
})

export default router;
