export default {
    getters : {
        IS_LOGGED_IN: state => !!state.auth.token,
        AUTH_STATUS: state => state.auth.status,
        AUTH_USER: state => state.users.user,
        TOKEN: state => state.auth.token,
    }
}
