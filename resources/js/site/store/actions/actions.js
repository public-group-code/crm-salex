import axios from "axios";
import store from "../store";
import login from "../../../auth/login";
import register from "../../../auth/register";
import logout from "../../../auth/logout";

const AUTH = {...login, ...register, ...logout}

export default {
    actions:{
        ...AUTH,
        GET_AUTH_USER_FROM_API({commit}){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/site/users/user',
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(resp => {
                        const user = resp.data.user;
                        commit('SET_AUTH_USER', {user});
                        return resolve(resp);
                    })
                    .catch(error => {
                        if (error.response.status === 401){
                            this.dispatch('LOGOUT');
                        }
                        return reject(error);
                    });
            })
        },
    },
}
