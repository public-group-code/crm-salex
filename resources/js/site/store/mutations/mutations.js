export default {
    mutations:{
        AUTH_REQUEST: (state) => {
            state.status = 'loading'
        },
        AUTH_SUCCESS: (state, {token}) =>{
            state.auth.status = 'authorized'
            state.auth.token = token
        },
        AUTH_ERROR: (state) => {
            state.auth.status = 'unauthorized'
        },
        LOGOUT: (state) => {
            state.auth.status = 'unauthorized'
            state.auth.token = ''
        },
        SET_AUTH_USER: (state, {user}) =>{
            state.users.user = user
        }
    },
}
