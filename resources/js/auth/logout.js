import axios from "axios";

export default {
    LOGOUT({commit}){
        return new Promise((resolve, reject) => {
            commit('LOGOUT')
            localStorage.removeItem('token')
            delete axios.defaults.headers.common['Authorization']
            resolve()
        })
    }
}
