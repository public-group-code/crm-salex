import axios from "axios";

export default {
    REGISTER({commit}, user){
        return new Promise((resolve, reject) => {
            commit('AUTH_REQUEST')
            axios.post(window.location.origin +'/api/v1/auth/register', {
                name: user.name.name,
                email: user.email.email,
                role: user.role.role,
                company_name: user.company_name.company_name,
                company_id: user.company_id.company_id,
                password: user.password.password,
                password_confirmation: user.password_confirmation.password_confirmation,
            })
                .then(resp => {
                    const token = resp.data.token
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('AUTH_SUCCESS', {token})
                    resolve(resp)
                })
                .catch(err => {
                    console.log(err.response)
                    commit('AUTH_ERROR', err)
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
}
