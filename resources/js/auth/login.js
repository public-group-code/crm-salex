import axios from "axios";

export default {
    LOGIN({commit}, {user, cabinet = null}) {
        return new Promise((resolve, reject) => {
            commit('AUTH_REQUEST')
            axios.post(window.location.origin + '/api/v1/auth/login', {
                email: user.email,
                password: user.password,
                remember_me: user.remember_me,
                cabinet: cabinet
            })
                .then(resp => {
                    const token = resp.data.token
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('AUTH_SUCCESS', {token})
                    resolve(resp)

                })
                .catch(err => {
                    commit('AUTH_ERROR')
                    localStorage.removeItem('token')
                    reject(err)
                    console.log(err.response.data)
                })
        })
    }
}
