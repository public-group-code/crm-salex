/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

window.Vue = require('vue').default;

import Vuelidate from 'vuelidate';
import router from './router/router.js';
import store from "./store/store";
import Pagination from 'vue-laravel-paginex'

Vue.use(Vuelidate);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
import App from './components/App.vue'

Vue.component('vHeader', require('./components/pages/partials/v-header').default)
Vue.component('vFooter', require('./components/pages/partials/v-footer').default)
Vue.component('Pagination', Pagination)
import 'vue-search-select/dist/VueSearchSelect.css'
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store,
});
