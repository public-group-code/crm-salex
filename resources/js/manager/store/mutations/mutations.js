export default {
    mutations:{
        //AUTH
        AUTH_REQUEST: (state) => {
            state.status = 'loading'
        },
        AUTH_SUCCESS: (state, {token}) =>{
            state.auth.status = 'authorized'
            state.auth.token = token
        },
        AUTH_ERROR: (state) => {
            state.auth.status = 'unauthorized'
        },
        LOGOUT: (state) => {
            state.auth.status = 'unauthorized'
            state.auth.token = ''
        },
        SET_AUTH_USER: (state, {user}) =>{
            state.users.user = user
        },
        //EMPLOYEES
        SET_EMPLOYEES: (state, {employees, pagination}) =>{
            state.employees.employees = employees
            state.employees.pagination = pagination
        },
        SET_EDIT_EMPLOYEE: (state, {employee}) =>{
            state.employees.employee = employee
        },
        //CUSTOMERS
        SET_CUSTOMERS: (state, {customers, pagination}) =>{
            state.customers.customers = customers
            state.customers.pagination = pagination
        },
        SET_EDIT_CUSTOMER: (state, {customer}) =>{
            state.customers.customer = customer
        },
    }
}
