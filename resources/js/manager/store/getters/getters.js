export default {
    getters : {
        //AUTH
        IS_LOGGED_IN: state => !!state.auth.token,
        TOKEN: state => state.auth.token,
        AUTH_STATUS: state => state.auth.status,
        AUTH_USER: state => state.users.user,
        //EMPLOYEES
        EMPLOYEES: state => state.employees.employees,
        EMPLOYEES_PAGINATION: state => state.employees.pagination,
        EDIT_EMPLOYEE: state => state.employees.employee,
        //CUSTOMERS
        CUSTOMERS: state => state.customers.customers,
        CUSTOMERS_PAGINATION: state => state.customers.pagination,
        EDIT_CUSTOMER: state => state.customers.customer,
    }
}
