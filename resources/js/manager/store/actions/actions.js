import axios from "axios";
import store from "../store";
import login from "../../../auth/login";
import register from "../../../auth/register";
import logout from "../../../auth/logout";

const AUTH = {...login, ...register, ...logout}

export default {
    actions:{
        //AUTH
        ...AUTH,
        GET_AUTH_USER_FROM_API({commit}){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/manager/users/user',
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(resp => {
                        const user = resp.data.user;
                        commit('SET_AUTH_USER', {user});
                        return resolve(resp);
                    })
                    .catch(error => {
                        if (error.response.status === 401){
                            this.dispatch('LOGOUT');
                        }
                        return reject(error);
                    });
            })
        },
        //employees
        GET_EMPLOYEES_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/manager/employees/employees?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const employees = response.data.employees.data;
                    const pagination = {
                        links: response.data.employees.links,
                        meta: response.data.employees.meta,
                    }
                    console.log('employees')
                    commit('SET_EMPLOYEES', {employees, pagination});
                })
                .catch(error => {
                    console.error(error.response)
                })
        },
        GET_EMPLOYEE_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/manager/employees/employees/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const employeeResp = response.data.employee;
                        const employee = {
                            id: {
                                validate:  'on',
                                id: employeeResp.id
                            },
                            name: {
                                validate:  'on',
                                name: employeeResp.name,
                            },
                            email: {
                                validate:  'on',
                                email: employeeResp.email,
                            },
                            phone: {
                                validate:  'on',
                                phone: employeeResp.phone,
                            },
                            roles: {
                                validate:  'on',
                                roles: Object.values(employeeResp.roles),
                            },
                            salary_type:{
                                validate:  'off',
                                salary_type: employeeResp.salary_type
                            },
                            salary_percent:{
                                validate:  'on',
                                salary_percent: employeeResp.salary_percent,
                            },
                            salary_rate:{
                                validate:  'on',
                                salary_rate: employeeResp.salary_rate,
                            },
                            password: {
                                validate:  'on',
                                password: null
                            },
                            password_confirmation: {
                                validate:  'on',
                                password_confirmation: null
                            },
                        }
                        commit('SET_EDIT_EMPLOYEE', {employee})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_EMPLOYEE_TO_API({commit}, employee){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/manager/employees/employees',
                    data: {
                        name: employee.name.name,
                        email: employee.email.email,
                        phone: employee.phone.phone,
                        roles: employee.roles.roles,
                        salary_type: employee.salary_type.salary_type,
                        salary_percent: employee.salary_percent.salary_percent,
                        salary_rate: employee.salary_rate.salary_rate,
                        password: employee.password.password,
                        password_confirmation: employee.password_confirmation.password_confirmation,
                    },
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                        console.error(error.response.data)
                    })
            })
        },
        UPDATE_EMPLOYEE_TO_API({commit}, employee){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/manager/employees/employees/' + employee.id.id,
                    data: {
                        name: employee.name.name,
                        email: employee.email.email,
                        phone: employee.phone.phone,
                        roles: employee.roles.roles,
                        salary_type: employee.salary_type.salary_type,
                        salary_percent: employee.salary_percent.salary_percent,
                        salary_rate: employee.salary_rate.salary_rate,
                        password: employee.password.password,
                        password_confirmation: employee.password_confirmation.password_confirmation,
                    },
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_EMPLOYEE_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/manager/employees/employees/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        //CUSTOMERS
        GET_CUSTOMERS_FROM_API({commit}, page = 1){
            axios({
                method: 'get',
                url: window.location.origin + '/api/v1/manager/customers/customers?page=' + page,
                headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
            })
                .then(response => {
                    const customers = response.data.customers.data;
                    const pagination = {
                        links: response.data.customers.links,
                        meta: response.data.customers.meta,
                    }
                    commit('SET_CUSTOMERS', {customers, pagination});
                })
                .catch(error => {
                    console.error(error.response.data)
                })
        },
        GET_CUSTOMER_BY_ID_FROM_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/manager/customers/customers/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const customerResp = response.data.customer;
                        const customer = {
                            id: customerResp.id,
                            first_name: customerResp.first_name,
                            last_name: customerResp.last_name,
                            phone: customerResp.phone,
                            mail_address: customerResp.mail_address,
                        }
                        commit('SET_EDIT_CUSTOMER', {customer})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        GET_CUSTOMER_BY_PHONE_FROM_API({commit}, phone){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'get',
                    url: window.location.origin + '/api/v1/manager/customers/customer/' + phone,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        const customerResp = response.data.customer;
                        const customer = {
                            id: customerResp.id,
                            first_name: customerResp.first_name,
                            last_name: customerResp.last_name,
                            phone: customerResp.phone,
                            mail_address: customerResp.mail_address,
                        }
                        commit('SET_EDIT_CUSTOMER', {customer})
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        STORE_CUSTOMER_TO_API({commit}, customer){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'POST',
                    url: window.location.origin + '/api/v1/manager/customers/customers',
                    data: customer,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        UPDATE_CUSTOMER_TO_API({commit}, customer){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'PUT',
                    url: window.location.origin + '/api/v1/manager/customers/customers/' + customer.id,
                    data: customer,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        DELETE_CUSTOMER_TO_API({commit}, id){
            return new Promise((resolve, reject) => {
                axios({
                    method: 'DELETE',
                    url: window.location.origin + '/api/v1/manager/customers/customers/' + id,
                    headers: {'Authorization': 'Bearer ' + store.getters.TOKEN}
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
    }
}
