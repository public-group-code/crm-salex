export default {
    state: {
        auth:{
            status: localStorage.getItem('token')? 'authorized' : 'unauthorized',
            token: localStorage.getItem('token') || '',
        },
        users:{
            user : {},
        },
        employees:{
            employees:[],
            employee:{},
            pagination:{}
        },
        customers:{
            customers:[],
            customer:{},
            pagination:{}
        }
    },
}
