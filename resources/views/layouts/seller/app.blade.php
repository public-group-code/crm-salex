<!doctype html>
<html lang="en">
    <head>
        <title>CRM-SALEX</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('builds/seller/css/app.css')}}">
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        <script src="{{asset('builds/seller/js/app.js')}}"></script>
    </body>
</html>
