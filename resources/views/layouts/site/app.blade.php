<!doctype html>
<html lang="en">
    <head>
        <title>CRM-SALEX</title>
        <link rel="stylesheet" href="{{asset('builds/site/css/app.css')}}">
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        <script src="{{asset('builds/site/js/app.js')}}"></script>
    </body>
</html>
