<!doctype html>
<html lang="en">
    <head>
        <title>CRM-SALEX</title>
        <link rel="stylesheet" href="{{asset('builds/admin/css/app.css')}}">
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
        <script src="{{asset('builds/admin/js/app.js')}}"></script>
    </body>
</html>
