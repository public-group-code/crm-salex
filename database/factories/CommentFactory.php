<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'order' => $this->faker->numberBetween(1, 10),
            'message' => $this->getComment(),
        ];
    }

    public function getComment(): string
    {

        $comments = ['Не известно', 'Нашли дешевле', 'Недобросовестный', 'Доволен', 'Не доволен'];

        return $comments[$this->faker->numberBetween(0, 4)];

    }
}
