<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->numberBetween(1, 10),
            'prepayment' => $this->faker->numberBetween(100, 1000),
            'cash_on_delivery' => $this->faker->numberBetween(100, 1000),
            'status' => $this->getStatus(),
            'comment_id' => $this->faker->numberBetween(1, 10),
            'profit' => $this->faker->numberBetween(100, 1000),
        ];
    }

    public function getStatus(): string
    {

        $statuses = ['Новый', 'Принят', 'Отклонен', 'Выполнен'];

        return $statuses[$this->faker->numberBetween(0, 3)];

    }
}
