<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'name' => $this->faker->title(),
            'purchase_price' => $this->faker->numberBetween(100, 200),
            'price' => $this->faker->numberBetween(250, 1000),
            'quantity' => $this->faker->numberBetween(100, 1000),
        ];
    }
}
