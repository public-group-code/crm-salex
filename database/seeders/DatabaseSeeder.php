<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::factory()->count(3)->state(new Sequence(
            [
                'name' => 'Admin',
                'slug' => 'admin',
            ],
            [
                'name' => 'Manager',
                'slug' => 'manager',
            ],
            [
                'name' => 'Seller',
                'slug' => 'seller',
            ],
        ))->create();
        \App\Models\User::factory(10)->create();
        \App\Models\Customer::factory(10)->create();
        \App\Models\Product::factory(10)->create();
        \App\Models\Comment::factory(10)->create();
        \App\Models\Order::factory(10)->create();
        \App\Models\Company::factory(3)->create();
    }
}
